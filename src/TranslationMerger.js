class TranslationMerger {
    merge(...translations) {
        if (translations.length === 1) {
            return translations[0]
        }
        const translationSets = {};
        translations.forEach(set => {
            Object.keys(set).forEach(lang => {
                if (typeof translationSets[lang] === 'undefined') {
                    translationSets[lang] = {};
                }
                const units = set[lang]
                for (const key in units) {
                    if (typeof translationSets[lang][key] === 'undefined') {
                        translationSets[lang][key] = units[key]
                    }
                }
            })
        })
        return translationSets;
    }
}

module.exports = TranslationMerger;
