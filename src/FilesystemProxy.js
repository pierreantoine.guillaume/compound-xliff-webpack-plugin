class FilesystemProxy {
    isCacheUpToDate(src, dest) {
        return this.fileIsMissing(dest) || (this.mtime(src) > this.mtime(dest))
    }

    mtime(src) {
    }

    fileIsMissing(file) {
    }

    read(cache) {
    }
}

class RealFileSystem extends FilesystemProxy {
    constructor(fs) {
        super();
        this.fs = fs;
    }

    fileIsMissing(file) {
        return !this.fs.existsSync(file);
    }

    read(cache) {
        return this.fs.readFileSync(cache)
    }

    mtime(src) {
        return this.fs.stat.mtime(src);
    }
}

module.exports = {FilesystemProxy, RealFileSystem}
