const {CacheFilePipeline, FromCacheFilePipeline} = require('./FilePipeline.js')

class PipelineFactory {
    /**
     * @param {FilesystemProxy} fs
     */
    constructor(fs) {
        this.fs = fs
    }

    create(src, dest) {
        if (!this.fs.isCacheUpToDate(src, dest)) {
            return new FromCacheFilePipeline(this.fs.read(dest))
        }
        return new CacheFilePipeline({src, dest})
    }
}

module.exports = PipelineFactory
