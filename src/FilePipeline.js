class FilePipeline {
  forward (importer, exporter) {
    return {}
  }
}

class FromCacheFilePipeline extends FilePipeline {
  constructor (cacheContent) {
    super()
    this.cacheContent = cacheContent
  }

  forward (importer, exporter) {
    return this.cacheContent
  }
}

class CacheFilePipeline extends FilePipeline {
  constructor ({ src, dest }) {
    super()
    this.src = src
    this.dest = dest
  }

  forward (importer, exporter) {
    const content = importer.import(this.src)
    exporter.export(this.dest, content)
    return content
  }
}

module.exports = { FilePipeline, CacheFilePipeline, FromCacheFilePipeline }
