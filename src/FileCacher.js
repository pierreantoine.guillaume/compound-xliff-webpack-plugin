class FileCacher {
  constructor ({ fileStateChecker: pipelineFactory, transformer, writer }) {
    this.pipelineFactory = pipelineFactory
    this.transformer = transformer
    this.writer = writer
  }

  /**
     * @param {string} source
     * @param {string} dest
     */
  cache ({ source, dest }) {
    this.pipelineFactory
      .create(source, dest)
      .forward(this.transformer, this.writer)
  }
}

module.exports = FileCacher
