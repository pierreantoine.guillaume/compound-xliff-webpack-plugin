class XLIFFParser {

    parse(jsonXliff) {
        const units = {};
        const file = jsonXliff.xliff.file
        const targetLanguage = file["target-language"] ?? file["source-language"];
        const from = targetLanguage === file["source-language"]
            ? (unit => unit.source)
            : (unit => unit.target)
        ;
        const transUnits = file.body["trans-unit"];
        if (Array.isArray(transUnits)) {
            transUnits.forEach(unit => {
                units[unit.resname] = from(unit)
            })
        } else {
            units[transUnits.resname] = from(transUnits)
        }
        return {[targetLanguage]: units};
    }
}

module.exports = XLIFFParser;
