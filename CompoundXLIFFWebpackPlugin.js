const pluginName = 'CompoundXLIFFWebpackPlugin'

const fs = require('fs')
const crypto = require('crypto')

/**
 * @param {string[]} array
 * @returns {Map<string, string>}
 */
const hashStrings = array => {
  /**
   * @type {Map<string, string>}
   */
  const map = new Map()
  array.forEach(string => {
    map.set(crypto.createHash('md5').update(string).digest('hex'), string)
  })
  return map
}

class CompoundXLIFFWebpackPlugin {
  /**
     * @param {string} buildLocation Where to build the final json representation in the assets ?
     * @param {string[]} xliffsToCompile Which XLIFF to compile ?
     * @param {string} cacheLocation Where to put cache file cache ?
     */
  constructor ({ buildLocation, xliffsToCompile, cacheLocation }) {
    this.buildLocation = buildLocation
    this.xliffsToCompile = xliffsToCompile.reverse()
    this.cacheLocation = cacheLocation
  }

  apply (compiler) {
    compiler.hooks.run.tap(pluginName, compilation => {
      const map = hashStrings(this.xliffsToCompile)
      map.forEach((path, hash) => {
        if (!fs.existsSync(`${this.cacheLocation}/${hash}`)) {
          fs.mkdirSync(`${this.cacheLocation}/${hash}`, { recursive: true })
        }
      })

      console.log(`${this.buildLocation} ; ` + this.xliffsToCompile.join(',') + ` ; ${this.cacheLocation}`)
    })
  }
}

module.exports = CompoundXLIFFWebpackPlugin

//  .addPlugin(
//             new XLIFFPlugin(
//                 {
//                     buildLocation: path.resolve(__dirname, `assets/translations/${tenant}`),
//                     xliffsToCompile: [
//                         path.resolve(__dirname, `translations/${tenant}`),
//                         path.resolve(__dirname, `translations/default`),
//                     ],
//                     cacheLocation: path.resolve(__dirname, 'var/node/translations')
//                 }
//             )
//         )
