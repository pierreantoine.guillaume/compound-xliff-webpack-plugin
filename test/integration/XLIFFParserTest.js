const XLIFFParser = require('../../src/XLIFFParser.js')

const parser = require('fast-xml-parser')

const assert = require('assert')

const {describe, it} = require("mocha");

function getXlfForTenantAndLang(xlf, lang) {
    const path = require('path')
    const fs = require('fs')
    const filePath = path.resolve(__dirname, `translations/${xlf}/messages.${lang}.xlf`);
    return fs.readFileSync(filePath, 'utf8').toString();
}

const parserOptions = {
    ignoreAttributes: false,
    attributeNamePrefix: ''
};

describe('XLIFFParser', () => {
    it("en default XLF should be parsed like so", () => {
        const xlfParser = new XLIFFParser();
        const enDefaultXlf = parser.convertToJson(parser.getTraversalObj(getXlfForTenantAndLang('default', 'en'), parserOptions), parserOptions);
        const keys = xlfParser.parse(enDefaultXlf)
        assert.deepStrictEqual(
            keys, {
                en: {
                    key1: 'first-source',
                    key2: 'second-source',
                    key3: 'third-source'
                }
            }
        )
    })
    it("fr default XLF should be parsed like so", () => {
        const xlfParser = new XLIFFParser();
        const frDefaultXlf = parser.convertToJson(parser.getTraversalObj(getXlfForTenantAndLang('default', 'fr'), parserOptions), parserOptions);
        const keys = xlfParser.parse(frDefaultXlf)

        assert.deepStrictEqual(keys, {
            fr: {
                key1: 'première-source',
                key2: 'seconde-source',
                key3: 'troisième-source'
            }
        })
    })
})
