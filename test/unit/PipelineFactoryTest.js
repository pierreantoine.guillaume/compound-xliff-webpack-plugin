const PipelineFactory = require('../../src/PipelineFactory.js')
const {FilesystemProxy} = require('../../src/FilesystemProxy')

const assert = require('assert')

const {describe, it} = require("mocha");

class InMemoryFilesystem extends FilesystemProxy {
    constructor() {
        super();
        this.files = new Map()
        this.internalCounter = 0;
    }

    create(file, content = "") {
        this.files.set(file, {mtime: ++this.internalCounter, content: content})
    }

    mtime(file) {
        return this.files.get(file).mtime
    }

    fileIsMissing(file) {
        return !this.files.has(file)
    }

    read(file) {
        return this.files.get(file).content
    }
}

class Importer {
    constructor(fs) {
        this.fs = fs;
    }

    import(file) {
        console.log(file)
        return this.fs.read(file)
    }
}

class Exporter {
    constructor(fs) {
        this.fs = fs;
    }

    export(file, content) {
        this.fs.create(file, content)
    }
}

describe('PipelineFactory', () => {
    it("Should create cache when no cache is present", () => {
        const fs = new InMemoryFilesystem();
        const factory = new PipelineFactory(fs);

        fs.create("file.fr.xlf")

        const cache = factory.create("file.fr.xlf", 'cache.json')
        cache.forward(new Importer(fs), new Exporter(fs))

        assert.strictEqual(fs.fileIsMissing('cache.json'), false)
    })
})
