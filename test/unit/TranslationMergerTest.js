const TranslationMerger = require('../../src/TranslationMerger.js')

const assert = require('assert')

const {describe, it} = require("mocha");

describe('TranslationMerger', () => {
    it("Should return the first item if nothing else provided", () => {
        const translationsInput = {
            lang: {
                a: 'a',
                b: 'b',
            }
        };

        const merger = new TranslationMerger();
        const mergedTranslations = merger.merge(translationsInput)
        assert.deepStrictEqual(mergedTranslations, {lang: {a: 'a', b: 'b'}})
    })
    it("Should use the first values first when multiple translation sets are provided", () => {

        const prioritaryInput = {lang: {b: 'c'}}
        const translationsInput = {
            lang: {
                a: 'a',
                b: 'b',
            }
        };

        const merger = new TranslationMerger();
        const mergedTranslations = merger.merge(prioritaryInput, translationsInput)
        assert.deepStrictEqual(mergedTranslations, {lang: {a: 'a', b: 'c'}})
    })
})
