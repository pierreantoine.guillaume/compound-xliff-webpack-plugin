const XLIFFParser = require('../../src/XLIFFParser.js')

const assert = require('assert')

const {describe, it} = require("mocha");

describe('XLIFFParser', () => {
    it("Should parse XLIFF", () => {
        const jsonXliff = {
            xliff: {
                version: '1.2',
                xmlns: 'urn:oasis:names:tc:xliff:document:1.2',
                file: {
                    'source-language': 'one',
                    datatype: 'plaintext',
                    original: 'file.ext',
                    body: {
                        'trans-unit': [
                            {id: '1', resname: 'key1', source: 'ta'},
                            {id: '2', resname: 'key2', source: 'tu'},
                        ]
                    }
                }
            }
        };
        const parser = new XLIFFParser();
        const result = parser.parse(jsonXliff)
        const {key1, key2} = result.one
        assert.strictEqual(key1, 'ta')
        assert.strictEqual(key2, 'tu')
    })
    it("Should parse XLIFF from another language", () => {
        const jsonXliff = {
            xliff: {
                version: '1.2',
                xmlns: 'urn:oasis:names:tc:xliff:document:1.2',
                file: {
                    'source-language': 'one',
                    'target-language': 'other',
                    datatype: 'plaintext',
                    original: 'file.ext',
                    body: {
                        'trans-unit': [
                            {
                                id: '1',
                                resname: 'key1',
                                source: 'ti',
                                target: 'ta'
                            },
                            {
                                id: '2',
                                resname: 'key2',
                                source: 'to',
                                target: 'tu'
                            },
                        ]
                    }
                }
            }
        };
        const parser = new XLIFFParser();
        const result = parser.parse(jsonXliff)
        const {key1, key2} = result.other
        assert.strictEqual(key1, 'ta')
        assert.strictEqual(key2, 'tu')
    })
})
